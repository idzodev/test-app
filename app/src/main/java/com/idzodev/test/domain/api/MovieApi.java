package com.idzodev.test.domain.api;

import android.support.annotation.Nullable;

import com.idzodev.test.domain.api.dto.MovieDto;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

/**
 * REST client for movies.
 */
public interface MovieApi {

    /**
     * Return list of movies
     *
     * @return {@link Observable} with list of {@link MovieDto}
     */
    @GET("movies.json")
    Observable<List<MovieDto>> getMovies();
}
