package com.idzodev.test.domain.mappers;

import com.google.gson.Gson;
import com.idzodev.test.R;
import com.idzodev.test.core.android.App;
import com.idzodev.test.domain.api.ApiError;
import com.idzodev.test.domain.exceptions.ApiException;
import com.idzodev.test.domain.exceptions.ServerException;

import java.io.IOException;

import javax.inject.Inject;

import okhttp3.ResponseBody;
import retrofit2.HttpException;

/**
 * Application exception mapper
 */
public class ExceptionMapper {

    private Gson gson;
    private App app;

    public ExceptionMapper(Gson gson, App app) {
        this.gson = gson;
        this.app = app;
    }

    /**
     * Handle all exception and mapped them into one of {@link com.idzodev.test.domain.exceptions.BaseException}
     * instance.
     *
     * @param throwable
     * @return
     */
    public Throwable handleError(Throwable throwable) {
        if (throwable instanceof HttpException){
            final ApiError apiError = getErrorFromResponse((HttpException) throwable);

            if (apiError == null)
                return new ServerException();


            return new ApiException(app.getString(R.string.failed), apiError.getMessage());
        }

        return throwable;
    }

    /**
     * Get API error response
     *
     * @param ex Http exception
     * @return Api error class that represent API JSON error schema
     */
    public ApiError getErrorFromResponse(HttpException ex){
        ResponseBody body = ex.response().errorBody();
        if (body != null){
            try {
                return gson.fromJson(body.string(), ApiError.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }
}