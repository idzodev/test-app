package com.idzodev.test.domain.entities;

import android.os.Parcelable;

import org.parceler.Parcel;

import java.util.List;

/**
 * Entity contain all available type for filtering movies. This class also implemented default
 * Android {@link Parcelable} functionality by using {@link org.parceler.Parcels}.
 */
@Parcel
public class MovieFilters {
    protected List<String> genresId;
    protected int releaseYear;
    protected float rating;

    public MovieFilters() {
    }

    public List<String> getGenresId() {
        return genresId;
    }

    public void setGenresId(List<String> genresId) {
        this.genresId = genresId;
    }

    public float getRating() {
        return rating;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}
