package com.idzodev.test.domain.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;

/**
 * ORM Entity mapped to table {@link MovieGenreDao#TABLENAME}.
 */
@Entity
public class MovieGenre {
    private String movieId;
    private String genreId;
    @Generated(hash = 869395601)
    public MovieGenre(String movieId, String genreId) {
        this.movieId = movieId;
        this.genreId = genreId;
    }
    @Generated(hash = 1900287297)
    public MovieGenre() {
    }
    public String getMovieId() {
        return this.movieId;
    }
    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }
    public String getGenreId() {
        return this.genreId;
    }
    public void setGenreId(String genreId) {
        this.genreId = genreId;
    }

}
