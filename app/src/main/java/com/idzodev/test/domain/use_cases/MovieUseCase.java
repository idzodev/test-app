package com.idzodev.test.domain.use_cases;

import com.idzodev.test.ui.dvo.movie.GenresDvo;
import com.idzodev.test.ui.dvo.movie.MoviesDvo;
import com.idzodev.test.ui.dvo.movie.GetMoviesDvo;

import io.reactivex.Observable;

/**
 * This class represents all business logic with movies and genres.
 */
public interface MovieUseCase {

    /**
     * Get movies.
     *
     * @param getMoviesDvo Class that represent how and where need to get movies.
     * @return {@link Observable} that emit movie data view object
     */
    Observable<MoviesDvo> getMovies(GetMoviesDvo getMoviesDvo);

    /**
     * Get genres of movies.
     *
     * @return {@link Observable} that emit genres data view object
     */
    Observable<GenresDvo> getAllGenres();
}
