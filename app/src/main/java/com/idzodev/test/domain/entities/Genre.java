package com.idzodev.test.domain.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

/**
 * ORM Entity mapped to table {@link GenreDao#TABLENAME}.
 */
@Entity
public class Genre {
    @Id
    private String id;
    private String name;
    @Generated(hash = 639545652)
    public Genre(String id, String name) {
        this.id = id;
        this.name = name;
    }
    @Generated(hash = 235763487)
    public Genre() {
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }


}
