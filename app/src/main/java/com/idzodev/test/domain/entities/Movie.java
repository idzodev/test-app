package com.idzodev.test.domain.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

import java.util.List;

/**
 * ORM Entity mapped to table {@link MovieDao#TABLENAME}.
 */
@Entity
public class Movie {
    @Id
    private String id;
    private String title;
    private String image;
    private Float rating;
    private Integer releaseYear;
    @Generated(hash = 919872049)
    public Movie(String id, String title, String image, Float rating,
            Integer releaseYear) {
        this.id = id;
        this.title = title;
        this.image = image;
        this.rating = rating;
        this.releaseYear = releaseYear;
    }
    @Generated(hash = 1263461133)
    public Movie() {
    }
    public String getId() {
        return this.id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getTitle() {
        return this.title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getImage() {
        return this.image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public Float getRating() {
        return this.rating;
    }
    public void setRating(Float rating) {
        this.rating = rating;
    }
    public Integer getReleaseYear() {
        return this.releaseYear;
    }
    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }
    
}
