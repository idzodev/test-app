package com.idzodev.test.domain.repositories.impl;


import com.idzodev.test.domain.api.dto.MovieDto;
import com.idzodev.test.domain.entities.DaoMaster;
import com.idzodev.test.domain.entities.DaoSession;
import com.idzodev.test.domain.entities.Genre;
import com.idzodev.test.domain.entities.Movie;
import com.idzodev.test.domain.entities.MovieGenre;
import com.idzodev.test.domain.mappers.MovieMapper;
import com.idzodev.test.domain.repositories.MovieRepository;
import com.idzodev.test.domain.repositories.SaveEntityRepository;

import org.greenrobot.greendao.database.Database;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link SaveEntityRepository} for saving all data.
 */
public class SaveEntityRepositoryImpl implements SaveEntityRepository {

    private DaoSession daoSession;
    private MovieMapper movieMapper;

    public SaveEntityRepositoryImpl(DaoSession daoSession, MovieMapper movieMapper) {
        this.daoSession = daoSession;
        this.movieMapper = movieMapper;
    }

    @Override
    public DaoSession getDaoSession() {
        return daoSession;
    }

    @Override
    public void save(MovieDto movieDto) {
        final Movie movie = movieMapper.map(movieDto);

        getDaoSession()
                .getMovieDao()
                .insertOrReplace(movie);

        if (movieDto.getGenre() == null || movieDto.getGenre().isEmpty()) return;

        for (String genreName : movieDto.getGenre()){
            final Genre genre = new Genre(genreName.toLowerCase().hashCode()+"", genreName);

            getDaoSession().getGenreDao()
                    .insertOrReplace(genre);

            getDaoSession().getMovieGenreDao()
                    .insertOrReplace(new MovieGenre(
                            movie.getId(),
                            genre.getId()));
        }
    }
}
