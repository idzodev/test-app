package com.idzodev.test.domain.repositories;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.idzodev.test.domain.api.dto.MovieDto;
import com.idzodev.test.domain.entities.Genre;
import com.idzodev.test.domain.entities.Movie;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.domain.entities.MovieWithGenres;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface that represents a Repository for getting {@link Movie} related data.
 */
public interface MovieRepository {

    /**
     * Get an {@link Observable} which will emit a List of {@link MovieWithGenres}.
     *
     * @param movieFilters Filters for movies
     * @return {@link Observable} that emit List of movies
     */
    Observable<List<MovieWithGenres>> getMoviesWithGenre(@NonNull MovieFilters movieFilters);

    /**
     * Save List of {@link MovieDto}
     *
     * @param movieDtos list of movies which need to save
     */
    void save(List<MovieDto> movieDtos);
}
