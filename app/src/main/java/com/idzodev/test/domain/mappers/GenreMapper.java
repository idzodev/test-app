package com.idzodev.test.domain.mappers;

import com.idzodev.test.domain.entities.Genre;
import com.idzodev.test.ui.dvo.movie.GenreItemDvo;
import com.idzodev.test.ui.dvo.movie.GenresDvo;

import java.util.ArrayList;
import java.util.List;

/**
 * Genre mapper
 */
public class GenreMapper {

    /**
     * Mapped genres entities to genres data view object
     *
     * @param genres genres entities
     * @return genres data view object
     */
    public GenresDvo map(List<Genre> genres) {
        List<GenreItemDvo> genresItemDvos = new ArrayList<>(genres.size());

        for (Genre genre : genres)
            genresItemDvos.add(new GenreItemDvo(genre.getId(), genre.getName()));

        return new GenresDvo(genresItemDvos);
    }
}
