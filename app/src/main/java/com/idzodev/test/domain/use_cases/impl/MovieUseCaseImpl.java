package com.idzodev.test.domain.use_cases.impl;

import com.idzodev.test.core.executors.PostExecutionThread;
import com.idzodev.test.core.executors.ThreadExecutor;
import com.idzodev.test.core.rx.RxErrorHandler;
import com.idzodev.test.core.rx.RxTransformers;
import com.idzodev.test.domain.api.MovieApi;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.domain.mappers.ExceptionMapper;
import com.idzodev.test.domain.mappers.GenreMapper;
import com.idzodev.test.domain.mappers.MovieMapper;
import com.idzodev.test.domain.repositories.GenreRepository;
import com.idzodev.test.domain.repositories.MovieRepository;
import com.idzodev.test.domain.use_cases.MovieUseCase;
import com.idzodev.test.ui.dvo.movie.GenresDvo;
import com.idzodev.test.ui.dvo.movie.MoviesDvo;
import com.idzodev.test.ui.dvo.movie.GetMoviesDvo;

import io.reactivex.Observable;
import retrofit2.Retrofit;

/**
 * Implementation of {@link MovieUseCase}
 */
public class MovieUseCaseImpl implements MovieUseCase {
    private ThreadExecutor threadExecutor;
    private PostExecutionThread postExecutionThread;
    private MovieApi movieApi;
    private MovieRepository movieRepository;
    private GenreRepository genreRepository;
    private ExceptionMapper exceptionMapper;
    private MovieMapper movieMapper;
    private GenreMapper genreMapper;

    public MovieUseCaseImpl(GenreRepository genreRepository, GenreMapper genreMapper, ThreadExecutor threadExecutor, PostExecutionThread postExecutionThread, Retrofit retrofit, MovieRepository movieRepository, ExceptionMapper exceptionMapper, MovieMapper movieMapper) {
        this.threadExecutor = threadExecutor;
        this.genreMapper = genreMapper;
        this.genreRepository = genreRepository;
        this.postExecutionThread = postExecutionThread;
        this.movieApi = retrofit.create(MovieApi.class);
        this.movieRepository = movieRepository;
        this.exceptionMapper = exceptionMapper;
        this.movieMapper = movieMapper;
    }

    @Override
    public Observable<MoviesDvo> getMovies(GetMoviesDvo getMoviesDvo) {
        Observable<MoviesDvo> getFromDB = Observable.empty();
        Observable<MoviesDvo> getFromApi = Observable.empty();

        if (getMoviesDvo.getLoad() == GetMoviesDvo.LOAD_DB_API || getMoviesDvo.getLoad() == GetMoviesDvo.LOAD_DB)
            getFromDB = getMoviesFromDB(getMoviesDvo.getFilters(), getMoviesDvo.getLoad() == GetMoviesDvo.LOAD_DB)
                    .compose(RxTransformers.applySchedulers(threadExecutor, postExecutionThread));

        if (getMoviesDvo.getLoad() == GetMoviesDvo.LOAD_DB_API || getMoviesDvo.getLoad() == GetMoviesDvo.LOAD_API)
            getFromApi = getMoviesFromApi(getMoviesDvo.getFilters())
                    .compose(RxTransformers.applySchedulers(threadExecutor, postExecutionThread));

        return Observable.concat(getFromDB, getFromApi);
    }

    @Override
    public Observable<GenresDvo> getAllGenres() {
        return genreRepository.getGenres()
                .map(genres -> genreMapper.map(genres))
                .compose(RxTransformers.applySchedulers(threadExecutor, postExecutionThread));
    }

    /**
     * Get movies from Database
     *
     * @param movieFilters filters for getting movies
     * @param isUpdated is future data updated
     * @return {@link Observable} that emit Movie data view object
     */
    private Observable<MoviesDvo> getMoviesFromDB(MovieFilters movieFilters, boolean isUpdated){
        return movieRepository.getMoviesWithGenre(movieFilters)
                .map(movies -> movieMapper.mapMovies(movies, isUpdated));
    }

    /**
     * Get movies from API
     *
     * @param movieFilters filters for getting movies
     * @return {@link Observable} that emit Movie data view object
     */
    private Observable<MoviesDvo> getMoviesFromApi(MovieFilters movieFilters){
        return movieApi.getMovies()
                .compose(RxErrorHandler.handleError(exceptionMapper))
                .doOnNext(movieDtos -> movieRepository.save(movieDtos))
                .flatMap(movieDtos -> getMoviesFromDB(movieFilters, true));
    }
}
