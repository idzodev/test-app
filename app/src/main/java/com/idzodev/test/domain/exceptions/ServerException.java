package com.idzodev.test.domain.exceptions;

/**
 * Exception caused by API if API server was unavailable or JSON schema type was wrong
 */
public class ServerException extends BaseException {
}
