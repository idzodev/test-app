package com.idzodev.test.domain.exceptions;

/**
 * Base application exception
 */
public abstract class BaseException extends Exception {
}
