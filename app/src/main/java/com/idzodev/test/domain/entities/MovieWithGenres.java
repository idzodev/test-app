package com.idzodev.test.domain.entities;

import java.util.List;

/**
 * Entity that contain movie information with all his genres.
 */
public class MovieWithGenres {
    private Movie movie;
    private List<Genre> genres;

    public MovieWithGenres(Movie movie, List<Genre> genres) {
        this.movie = movie;
        this.genres = genres;
    }

    public Movie getMovie() {
        return movie;
    }

    public List<Genre> getGenres() {
        return genres;
    }
}
