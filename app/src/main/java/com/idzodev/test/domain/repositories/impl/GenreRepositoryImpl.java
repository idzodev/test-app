package com.idzodev.test.domain.repositories.impl;

import com.idzodev.test.domain.entities.Genre;
import com.idzodev.test.domain.entities.GenreDao;
import com.idzodev.test.domain.repositories.GenreRepository;
import com.idzodev.test.domain.repositories.SaveEntityRepository;

import java.util.List;

import io.reactivex.Observable;

/**
 * {@link GenreRepository} for retrieving genre data.
 */
public class GenreRepositoryImpl implements GenreRepository {
    private SaveEntityRepository saveEntityRepository;

    public GenreRepositoryImpl(SaveEntityRepository saveEntityRepository) {
        this.saveEntityRepository = saveEntityRepository;
    }

    @Override
    public Observable<List<Genre>> getGenres() {
        return Observable.create(e -> {

            List<Genre> genres = saveEntityRepository.getDaoSession()
                    .getGenreDao()
                    .queryBuilder()
                    .orderAsc(GenreDao.Properties.Name)
                    .list();

            e.onNext(genres);
            e.onComplete();
        });
    }
}
