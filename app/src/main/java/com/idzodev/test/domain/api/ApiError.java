package com.idzodev.test.domain.api;

/**
 * Represent Api Exception JSON schema
 */
public class ApiError {
    private String message;

    public String getMessage() {
        return message;
    }
}
