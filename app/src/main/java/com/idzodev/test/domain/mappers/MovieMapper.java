package com.idzodev.test.domain.mappers;

import com.idzodev.test.domain.api.dto.MovieDto;
import com.idzodev.test.domain.entities.MovieWithGenres;
import com.idzodev.test.domain.entities.Genre;
import com.idzodev.test.domain.entities.Movie;
import com.idzodev.test.ui.dvo.movie.GenreItemDvo;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;
import com.idzodev.test.ui.dvo.movie.MoviesDvo;

import java.util.ArrayList;
import java.util.List;

/**
 * Movie mapper
 */
public class MovieMapper {
    /**
     * Mapped movies with genres to movie data view object
     *
     * @param movies movies with genres
     * @param isUpdated if movies update from server
     * @return movie data view object
     */
    public MoviesDvo mapMovies(List<MovieWithGenres> movies, boolean isUpdated) {
        final List<MovieItemDvo> movieItemDvos = new ArrayList<>(movies.size());

        for (MovieWithGenres movie : movies){
            final List<GenreItemDvo> moviewGenres = new ArrayList<>();
            for (Genre genre : movie.getGenres()){
                moviewGenres.add(new GenreItemDvo(genre.getId(), genre.getName()));
            }
            movieItemDvos.add(new MovieItemDvo(
                    movie.getMovie().getTitle(),
                    movie.getMovie().getImage(),
                    movie.getMovie().getRating(),
                    movie.getMovie().getReleaseYear(),
                    moviewGenres
            ));
        }

        return new MoviesDvo(movieItemDvos, isUpdated);
    }

    /**
     * Mapped movie data transfer object to movie entity
     *
     * @param movieDto movie data transfer object
     * @return movie entity
     */
    public Movie map(MovieDto movieDto) {
        Movie movie = new Movie();
        movie.setId(movieDto.getId());
        movie.setImage(movieDto.getImage());
        movie.setTitle(movieDto.getTitle());
        movie.setRating(movieDto.getRating());
        movie.setReleaseYear(movieDto.getReleaseYear());
        return movie;
    }
}
