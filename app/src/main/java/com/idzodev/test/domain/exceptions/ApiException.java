package com.idzodev.test.domain.exceptions;

import com.idzodev.test.domain.api.ApiError;

/**
 * Exception caused by API
 */
public class ApiException extends BaseException {
    private String title;
    private String message;

    public ApiException(String title, String message) {
        this.title = title;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public String getTitle() {
        return title;
    }
}
