package com.idzodev.test.domain.api.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Movie data transfer class that represent Movie JSON schema
 */
public class MovieDto {
    String title;
    String image;
    Float rating;
    Integer releaseYear;
    List<String> genre;

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public Float getRating() {
        return rating;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public List<String> getGenre() {
        if (genre == null) return new ArrayList<>();

        return genre;
    }

    /**
     * Generate id from {@link MovieDto#title}
     *
     * @return generated id
     */
    public String getId() {
        return title.toLowerCase().hashCode()+"";
    }
}
