package com.idzodev.test.domain.repositories;


import com.idzodev.test.domain.api.dto.MovieDto;
import com.idzodev.test.domain.entities.DaoSession;
import com.idzodev.test.domain.entities.Movie;

/**
 * Interface that represents a Repository for saving all data.
 */
public interface SaveEntityRepository {
    /**
     * Get database instance
     *
     * @return instance of database
     */
    DaoSession getDaoSession();

    /**
     * Save {@link MovieDto} and all related data.
     *
     * @param movieDto Movie data transfer object
     */
    void save(MovieDto movieDto);
}
