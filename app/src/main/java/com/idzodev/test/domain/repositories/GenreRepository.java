package com.idzodev.test.domain.repositories;

import com.idzodev.test.domain.entities.Genre;

import java.util.List;

import io.reactivex.Observable;

/**
 * Interface that represents a Repository for getting {@link Genre} related data.
 */
public interface GenreRepository {

    /**
     * Get an {@link Observable} which will emit a List of {@link Genre}.
     *
     * @return
     */
    Observable<List<Genre>> getGenres();
}
