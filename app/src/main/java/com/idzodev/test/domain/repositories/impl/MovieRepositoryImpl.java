package com.idzodev.test.domain.repositories.impl;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.LongSparseArray;

import com.idzodev.test.core.utils.StringUtils;
import com.idzodev.test.domain.api.dto.MovieDto;
import com.idzodev.test.domain.entities.Genre;
import com.idzodev.test.domain.entities.GenreDao;
import com.idzodev.test.domain.entities.Movie;
import com.idzodev.test.domain.entities.MovieDao;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.domain.entities.MovieGenre;
import com.idzodev.test.domain.entities.MovieGenreDao;
import com.idzodev.test.domain.entities.MovieWithGenres;
import com.idzodev.test.domain.repositories.GenreRepository;
import com.idzodev.test.domain.repositories.MovieRepository;
import com.idzodev.test.domain.repositories.SaveEntityRepository;

import org.greenrobot.greendao.query.QueryBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * {@link MovieRepository} for retrieving movie data.
 */
public class MovieRepositoryImpl implements MovieRepository {
    private SaveEntityRepository saveEntityRepository;

    public MovieRepositoryImpl(SaveEntityRepository saveEntityRepository) {
        this.saveEntityRepository = saveEntityRepository;
    }

    @Override
    public Observable<List<MovieWithGenres>> getMoviesWithGenre(@NonNull MovieFilters movieFilters) {
        return Observable.create(e -> {
            final StringBuilder selectMoviesBuilder = new StringBuilder("select movies.* from %1$s as movies ");

            // Filters
            //Genres filters
            boolean hasWhere = false;
            if (movieFilters.getGenresId() != null && !movieFilters.getGenresId().isEmpty()){
                final String subSelectHasGenre = "select * from %1$s as filmGender WHERE filmGender.%2$s =  movies.%3$s AND filmGender.%4$s = '%5$s'";
                selectMoviesBuilder.append(" WHERE ");
                hasWhere = true;

                for (int i = 0; i < movieFilters.getGenresId().size(); i++) {
                    if (i != 0){
                        selectMoviesBuilder.append(" AND ");
                    }

                    selectMoviesBuilder.append(" EXISTS (")
                            .append(String.format(subSelectHasGenre,
                                    MovieGenreDao.TABLENAME,
                                    MovieGenreDao.Properties.MovieId.columnName,
                                    MovieDao.Properties.Id.columnName,
                                    MovieGenreDao.Properties.GenreId.columnName,
                                    movieFilters.getGenresId().get(i)
                            ))
                            .append(")");
                }
            }

            // Release year filters
            if (movieFilters.getReleaseYear() != 0){
                selectMoviesBuilder.append( hasWhere ? " AND " : " WHERE ");
                hasWhere = true;
                selectMoviesBuilder.append(" movies.%4$s = ").append(movieFilters.getReleaseYear()).append("");
            }

            // Rating filters
            if (movieFilters.getRating() != 0){
                selectMoviesBuilder.append( hasWhere ? " AND " : " WHERE ");
                hasWhere = true;
                selectMoviesBuilder.append(" movies.%5$s >").append(movieFilters.getRating()-0.1f)
                        .append(" AND movies.%5$s < ").append(movieFilters.getRating()+0.1f);
            }

            selectMoviesBuilder.append(" GROUP BY movies.%6$s");
            selectMoviesBuilder.append(" ORDER BY movies.").append(MovieDao.Properties.ReleaseYear.columnName).append(" DESC, ")
                    .append(" movies.").append(MovieDao.Properties.Title.columnName);

            Cursor cursor = saveEntityRepository.getDaoSession()
                    .getMovieDao()
                            .getDatabase()
                            .rawQuery(String.format(selectMoviesBuilder.toString(),
                                    MovieDao.TABLENAME,
                                    MovieGenreDao.TABLENAME,
                                    MovieGenreDao.Properties.MovieId.columnName,
                                    MovieDao.Properties.ReleaseYear.columnName,
                                    MovieDao.Properties.Rating.columnName,
                                    MovieDao.Properties.Id.columnName

                            ), null);

            final List<Movie> movies = new ArrayList<Movie>(cursor.getCount());
            if (cursor.moveToFirst()){
                do {
                    Movie movie = saveEntityRepository.getDaoSession()
                            .getMovieDao()
                            .readEntity(cursor, 0);

                    movies.add(movie);
                } while (cursor.moveToNext());
            }
            cursor.close();

            final List<String> ids = new ArrayList<>(movies.size());
            for (Movie movie : movies)
                ids.add(movie.getId());

            //select movies genres
            final String selectGenres = "select genres.*, movieGenre.%5$s as movie_id from %1$s as genres INNER JOIN %2$s as movieGenre ON movieGenre.%3$s = genres.%4$s WHERE movieGenre.%5$s IN (%6$s)";

            cursor = saveEntityRepository.getDaoSession().getDatabase()
                    .rawQuery(String.format(selectGenres,
                            GenreDao.TABLENAME,
                            MovieGenreDao.TABLENAME,
                            MovieGenreDao.Properties.GenreId.columnName,
                            GenreDao.Properties.Id.columnName,
                            MovieGenreDao.Properties.MovieId.columnName,
                            StringUtils.join(ids)
                    ), null);

            final Map<String, List<Genre>> movieGenres = new HashMap<>(movies.size());
            if (cursor.moveToFirst()){
                do {
                    final Genre genre = saveEntityRepository.getDaoSession()
                            .getGenreDao()
                            .readEntity(cursor, 0);

                    final String movieId = cursor.getString(cursor.getColumnIndex("movie_id"));

                    List<Genre> genres = movieGenres.get(movieId);
                    if (genres == null) genres = new ArrayList<>();
                    genres.add(genre);
                    movieGenres.put(movieId, genres);
                } while (cursor.moveToNext());
            }
            cursor.close();


            final List<MovieWithGenres> movieWithGenres = new ArrayList<>(movies.size());
            for (Movie movie : movies){
                List<Genre> genres = movieGenres.get(movie.getId());
                if (genres == null) genres = new ArrayList<>();
                movieWithGenres.add(new MovieWithGenres(movie, genres));
            }

            e.onNext(movieWithGenres);
            e.onComplete();
        });
    }

    @Override
    public void save(List<MovieDto> movieDtos) {
        if (movieDtos == null || movieDtos.isEmpty()) return;

        saveEntityRepository.getDaoSession().runInTx(() -> {
            clearMovieData();
            for (MovieDto movie: movieDtos)
                saveEntityRepository.save(movie);
        });
    }

    /**
     * Remove old cached data
     */
    private void clearMovieData(){
        saveEntityRepository.getDaoSession()
                .getMovieGenreDao()
                .queryBuilder()
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();

        saveEntityRepository.getDaoSession()
                .getMovieDao()
                .queryBuilder()
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();


        saveEntityRepository.getDaoSession()
                .getGenreDao()
                .queryBuilder()
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }
}
