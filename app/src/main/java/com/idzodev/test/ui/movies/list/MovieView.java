package com.idzodev.test.ui.movies.list;

import com.idzodev.test.core.mvp.PopupView;
import com.idzodev.test.core.mvp.Presenter;
import com.idzodev.test.core.mvp.View;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;

import java.util.List;

/**
 * Movies {@link View} that controlled by {@link MoviesPresenter}.
 */
public interface MovieView extends View, PopupView {

    /**
     * Show list of movies
     *
     * @param movieItems list of {@link MovieItemDvo}
     */
    void showMovies(List<MovieItemDvo> movieItems);

    /**
     * Show loading progress
     *
     * @param show true if it need to show
     */
    void showProgress(boolean show);

    /**
     * Open movies filters screen.
     *
     * @param movieFilters old selected movies filters
     * @param requestFilters request code
     */
    void showFiltersScreen(MovieFilters movieFilters, int requestFilters);
}
