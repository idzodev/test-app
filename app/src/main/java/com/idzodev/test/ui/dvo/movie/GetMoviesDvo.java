package com.idzodev.test.ui.dvo.movie;

import android.annotation.SuppressLint;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.idzodev.test.domain.entities.MovieFilters;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

/**
 * Class that determine how and where need to get movies from {@link com.idzodev.test.domain.use_cases.MovieUseCase}.
 */
public class GetMoviesDvo {
    /**
     * Load movies from Database and then from API
     */
    public static final int LOAD_DB_API = 1;
    /**
     * Load movies only from API
     */
    public static final int LOAD_API = 2;
    /**
     * Load movies only from DB
     */
    public static final int LOAD_DB = 3;

    /**
     * {@linkplain IntDef} annotation for loading type.
     * @see #LOAD_DB_API
     * @see #LOAD_API
     * @see #LOAD_DB
     * @see #getLoad()
     */
    @SuppressLint("UniqueConstants")
    @Retention(RetentionPolicy.RUNTIME)
    @IntDef(flag = true, value = {
            LOAD_API, LOAD_DB, LOAD_DB_API
    })
    public @interface LoadMovies {
    }

    private MovieFilters filters;

    @LoadMovies
    private int load;

    /**
     * @param filters Movies filters {@link MovieFilters}
     * @param load int flag for defining loading type {@link LoadMovies} of movies
     */
    public GetMoviesDvo(MovieFilters filters, @LoadMovies int load) {
        this.filters = filters;
        this.load = load;
    }

    /**
     * @return int of flags used for loading type of movies
     * @see #LOAD_DB_API
     * @see #LOAD_API
     * @see #LOAD_DB
     */
    @LoadMovies
    public int getLoad() {
        return load;
    }

    public MovieFilters getFilters() {
        return filters;
    }
}
