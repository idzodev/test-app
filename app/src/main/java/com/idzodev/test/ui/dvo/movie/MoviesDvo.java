package com.idzodev.test.ui.dvo.movie;

import java.util.List;

/**
 * Movies data view object
 */
public class MoviesDvo {
    private List<MovieItemDvo> movieItems;
    private boolean isUpdated;

    public MoviesDvo(List<MovieItemDvo> movieItems, boolean isUpdated) {
        this.movieItems = movieItems;
        this.isUpdated = isUpdated;
    }

    public List<MovieItemDvo> getMovieItems() {
        return movieItems;
    }

    /**
     * Is movies updated from API
     *
     * @return true if its updated from API
     */
    public boolean isUpdated() {
        return isUpdated;
    }
}
