package com.idzodev.test.ui.movies.list;

import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.idzodev.test.R;
import com.idzodev.test.core.utils.ImageLoader;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * ViewHolder of Movie item in list.
 */
public class MovieViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.movie_item_name)
    protected TextView vName;
    @BindView(R.id.movie_item_genre)
    protected TextView vGenre;
    @BindView(R.id.movie_item_score)
    protected TextView vRating;
    @BindView(R.id.movie_item_premiere)
    protected TextView vReleaseDate;
    @BindView(R.id.movie_item_image)
    protected ImageView vImage;

    public MovieViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void show(MovieItemDvo dvo, int pos, View.OnClickListener onClickListener) {
        vName.setText(dvo.getTitle());
        vGenre.setText(dvo.getFormatGenreText());
        vReleaseDate.setText(itemView.getContext().getString(R.string.premiere_number, dvo.getReleaseYear()));
        vRating.setText(itemView.getContext().getString(R.string.score_number, dvo.getRating()));

        ImageLoader.getInstance().loadMovie(vImage, dvo.getImage());

        itemView.setOnClickListener(onClickListener);
        itemView.setTag(pos);
    }
}

