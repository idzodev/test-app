package com.idzodev.test.ui.movies.filters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idzodev.test.R;
import com.idzodev.test.ui.dvo.movie.GenreItemDvo;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Adapter that manages a collection of {@link GenreItemDvo}.
 */
public class FiltersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final static String SAVED_STATE_SELECTED = "SAVED_STATE_SELECTED";
    private final static String SAVED_STATE_ITEMS = "SAVED_STATE_ITEMS";

    private final static int HEADER = 1;
    private final static int BODY = 2;

    private final LayoutInflater inflater;

    private List<GenreItemDvo> items;
    private final Set<String> selectedGenres = new HashSet<>();

    private View vHeader;

    public FiltersAdapter(Context context, View vHeader) {
        this.inflater = LayoutInflater.from(context);
        this.vHeader = vHeader;
        this.items = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case HEADER: return new FiltersHeaderViewHolder(vHeader);
            case BODY: return new FiltersViewHolder(inflater.inflate(R.layout.filter_genre_item, parent, false));
        }

        throw new RuntimeException(" FiltersAdapter.onCreateViewHolder exception ");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        final int type = getItemViewType(position);
        if (type == BODY){
            position--;

            FiltersViewHolder holder = (FiltersViewHolder) h;
            holder.show(items.get(position), selectedGenres.contains(items.get(position).getId()), item -> {
                if (selectedGenres.contains(item.getId()))
                    selectedGenres.remove(item.getId());
                else
                    selectedGenres.add(item.getId());
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? HEADER : BODY;
    }

    @Override
    public int getItemCount() {
        return items.size()+1;
    }

    /**
     * Show new genres filters items and {@link RecyclerView.Adapter#notifyDataSetChanged()}
     *
     * @param dvo list genres
     * @param selectedGenres list of selected genres
     */
    public void showNewItems(List<GenreItemDvo> dvo, List<String> selectedGenres) {
        this.selectedGenres.clear();
        this.items.clear();
        if (selectedGenres != null)
            this.selectedGenres.addAll(selectedGenres);
        if (dvo != null)
            this.items.addAll(dvo);

        notifyDataSetChanged();
    }

    public Set<String> getSelectedGenres() {
        return selectedGenres;
    }

    /**
     * Restore adapter state
     *
     * @param savedInstanceState Bundle from where restore saved state
     */
    public void onRestoreState(Bundle savedInstanceState) {
        if (savedInstanceState == null) return;

        final List<String> savedSelected = Parcels.unwrap(savedInstanceState.getParcelable(SAVED_STATE_SELECTED));
        final List<GenreItemDvo> savedGenres = Parcels.unwrap(savedInstanceState.getParcelable(SAVED_STATE_ITEMS));

        showNewItems(savedGenres, savedSelected);
    }

    /**
     * Save adapter state
     *
     * @param outState {@link Bundle} to save state
     */
    public void saveInstanceState(Bundle outState) {
        outState.putParcelable(SAVED_STATE_ITEMS, Parcels.wrap(items));
        outState.putParcelable(SAVED_STATE_SELECTED, Parcels.wrap(new ArrayList<>(selectedGenres)));
    }
}

