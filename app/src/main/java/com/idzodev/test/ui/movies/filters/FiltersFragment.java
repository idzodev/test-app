package com.idzodev.test.ui.movies.filters;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idzodev.test.R;
import com.idzodev.test.core.android.App;
import com.idzodev.test.core.mvp.MvpFragment;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.ui.di.FiltersComponent;
import com.idzodev.test.ui.dvo.movie.GenreItemDvo;
import com.idzodev.test.ui.widgets.MovieFiltersHeaderWidget;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment that shows a list of movies filters
 */
public class FiltersFragment extends MvpFragment<FiltersPresenter> implements FiltersView {
    public static final String ARGS_SELECTED_FILTERS = "ARGS_SELECTED_FILTERS";
    private static final String SAVED_STATE_RECYCLER_POSITION = "SAVED_STATE_RECYCLER_POSITION";


    @BindView(R.id.filters_fragment_list)
    protected RecyclerView vList;
    @BindView(R.id.toolbar)
    protected Toolbar vToolbar;

    private MovieFiltersHeaderWidget vMovieFiltersHeaderWidget;

    @Inject
    protected FiltersPresenter filtersPresenter;

    private FiltersAdapter mFiltersAdapter;

    public static FiltersFragment newInstance(MovieFilters mMovieFilters) {
        Bundle args = new Bundle();
        args.putParcelable(ARGS_SELECTED_FILTERS, Parcels.wrap(mMovieFilters));
        FiltersFragment fragment = new FiltersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public FiltersPresenter setupPresenter() {
        App.get(getContext())
                .getAppComponent()
                .plus(new FiltersComponent.Module(
                        Parcels.unwrap(getArguments().getParcelable(ARGS_SELECTED_FILTERS))
                ))
                .inject(this);

        return filtersPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.filters_fragment, container, false);
        ButterKnife.bind(this, view);
        setupToolbar(vToolbar, R.string.filters, R.drawable.ic_arrow_white, view1 -> getPresenter().onFinishSelected());

        vList.setLayoutManager(new LinearLayoutManager(getContext()));
        vMovieFiltersHeaderWidget = (MovieFiltersHeaderWidget) inflater.inflate(R.layout.filter_header_item, vList, false);
        vList.setAdapter(mFiltersAdapter = new FiltersAdapter(getContext(), vMovieFiltersHeaderWidget));

        if (savedInstanceState != null){
            mFiltersAdapter.onRestoreState(savedInstanceState);
            vMovieFiltersHeaderWidget.onRestore(savedInstanceState);
            vList.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(SAVED_STATE_RECYCLER_POSITION));
        }

        getPresenter().attachView(this);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_STATE_RECYCLER_POSITION, vList.getLayoutManager().onSaveInstanceState());
        mFiltersAdapter.saveInstanceState(outState);
        vMovieFiltersHeaderWidget.saveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().detachView();
    }

    @Override
    public void showGenres(List<GenreItemDvo> movieItems, List<String> selectedGenres) {
        mFiltersAdapter.showNewItems(movieItems, selectedGenres);
    }

    @Override
    public void showRating(float rating) {
        vMovieFiltersHeaderWidget.setRating(rating);
    }

    @Override
    public void showReleaseYear(int releaseYear) {
        vMovieFiltersHeaderWidget.setYear(releaseYear);
    }

    @Override
    public MovieFilters getSelectedFilters() {
        MovieFilters movieFilters = new MovieFilters();
        movieFilters.setGenresId(new ArrayList<>(mFiltersAdapter.getSelectedGenres()));
        movieFilters.setRating(vMovieFiltersHeaderWidget.getScore());
        movieFilters.setReleaseYear(vMovieFiltersHeaderWidget.getReleaseYear());
        return movieFilters;
    }

    @Override
    public void close(int result, Intent data) {
        getTargetFragment().onActivityResult(getTargetRequestCode(), result, data);
        getActivity().getSupportFragmentManager()
                .popBackStack();
    }

    @Override
    public void onBackPressed() {
        getPresenter().onFinishSelected();
        super.onBackPressed();
    }
}
