package com.idzodev.test.ui.movies;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.idzodev.test.R;
import com.idzodev.test.core.android.BaseActivity;
import com.idzodev.test.core.mvp.MvpFragment;
import com.idzodev.test.ui.movies.list.MoviesFragment;

/**
 * Main application screen. This is the app entry point.
 */
public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.movie_activity);


        if (getSupportFragmentManager().findFragmentById(R.id.fragment_container) == null){

            if (savedInstanceState != null) return;

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, new MoviesFragment(), MoviesFragment.class.getName())
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            ((MvpFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_container))
                    .onBackPressed();
            return;
        }
        super.onBackPressed();
    }
}
