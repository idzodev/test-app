package com.idzodev.test.ui.movies.list;

import com.idzodev.test.core.mvp.Presenter;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;

/**
 * Movies {@link Presenter} that controls communication between {@link MovieView} and use case.
 */
public interface MoviesPresenter extends Presenter<MovieView> {
    /**
     * Handle clicked by movie
     *
     * @param item movie
     */
    void onMovieClicked(MovieItemDvo item);

    /**
     * Handle clicked on refresh list
     */
    void onRefreshClicked();

    /**
     * Handle clicked on filters
     */
    void onFilterClicked();
}
