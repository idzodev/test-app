package com.idzodev.test.ui.widgets;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSeekBar;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.idzodev.test.R;
import com.idzodev.test.core.utils.StringUtils;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Header view in filters list. Contain two EditTexts for set/get movie rating and release year.
 */
public class MovieFiltersHeaderWidget extends CardView {
    private static final String SAVED_STATE_RELEASE_DATE = "SAVED_STATE_RELEASE_DATE";
    private static final String SAVED_STATE_RATING = "SAVED_STATE_RATING";

    @BindView(R.id.filter_header_item_release_date_text)
    protected EditText vReleaseDateText;
    @BindView(R.id.filter_header_item_rating_text)
    protected EditText vRatingText;


    public MovieFiltersHeaderWidget(Context context) {
        super(context);
    }

    public MovieFiltersHeaderWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this, this);
    }

    /**
     * Get entered release year for filtering movie.
     *
     * @return movie release year
     */
    public int getReleaseYear(){
        String year = vReleaseDateText.getText().toString();
        if (StringUtils.isNullEmpty(year)) return 0;

        return Integer.parseInt(year);
    }

    /**
     * Get entered rating for filtering movie
     *
     * @return movie rating
     */
    public float getScore(){
        String score = vRatingText.getText().toString();
        if (StringUtils.isNullEmpty(score)) return 0;

        return Float.parseFloat(score);
    }

    /**
     * Set movie rating to show
     *
     * @param score movie rating
     */
    public void setRating(float score){
        if (score == 0){
            vRatingText.setText("");
            return;
        }

        vRatingText.setText(String.valueOf(score));
    }

    /**
     * Set movie release year to show
     *
     * @param year movie release year
     */
    public void setYear(int year){
        if (year == 0){
            vReleaseDateText.setText("");
            return;
        }

        vReleaseDateText.setText(String.valueOf(year));
    }

    /**
     * Save view state
     *
     * @param outState {@link Bundle} to save state
     */
    public void saveInstanceState(Bundle outState) {
        outState.putFloat(SAVED_STATE_RATING, getScore());
        outState.putInt(SAVED_STATE_RELEASE_DATE, getReleaseYear());
    }

    /**
     * Restore saved view state
     *
     * @param savedInstanceState {@link Bundle} from where can restore state
     */
    public void onRestore(Bundle savedInstanceState) {
        setYear(savedInstanceState.getInt(SAVED_STATE_RELEASE_DATE,0));
        setRating(savedInstanceState.getFloat(SAVED_STATE_RATING,0f));
    }
}
