package com.idzodev.test.ui.movies.filters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.idzodev.test.core.android.App;
import com.idzodev.test.core.mvp.BasePresenter;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.domain.use_cases.MovieUseCase;

import org.parceler.Parcels;

/**
 * Implementation of {@link FiltersPresenter}
 */
public class FiltersPresenterImpl extends BasePresenter<FiltersView> implements FiltersPresenter {

    private final MovieFilters selectedFilters;
    private MovieUseCase movieUseCase;
    private App app;

    public FiltersPresenterImpl(MovieFilters selectedFilters, MovieUseCase movieUseCase, App app) {
        this.selectedFilters = selectedFilters;
        this.movieUseCase = movieUseCase;
        this.app = app;
    }

    @Override
    protected void handleError(Throwable throwable) {
        handleDefaultsErrors(app, throwable);

        if (getView() == null) return;


    }

    @Override
    protected void onViewAttached(Bundle savedInstanceState) {
        if (savedInstanceState != null) return;

        addDisposable(movieUseCase.getAllGenres().subscribe(genresDvo -> {
            if (getView() == null) return;

            getView().showGenres(genresDvo.getGenres(), selectedFilters.getGenresId());
            getView().showRating(selectedFilters.getRating());
            getView().showReleaseYear(selectedFilters.getReleaseYear());
        }, this::handleError));
    }

    @Override
    public void onFinishSelected() {
        MovieFilters movieFilters = getView().getSelectedFilters();
        Intent intent = new Intent();
        intent.putExtra(FiltersFragment.ARGS_SELECTED_FILTERS, Parcels.wrap(movieFilters));
        getView().close(Activity.RESULT_OK, intent);
    }
}
