package com.idzodev.test.ui.di;

import com.idzodev.test.core.android.App;
import com.idzodev.test.core.di.scope.PerFragment;
import com.idzodev.test.domain.use_cases.MovieUseCase;
import com.idzodev.test.ui.movies.list.MoviesFragment;
import com.idzodev.test.ui.movies.list.MoviesPresenter;
import com.idzodev.test.ui.movies.list.MoviesPresenterImpl;

import dagger.Provides;
import dagger.Subcomponent;

/**
 * Movies component for {@link MoviesFragment}
 */
@PerFragment
@Subcomponent(modules = MovieComponent.Module.class)
public interface MovieComponent {
    void inject(MoviesFragment moviesFragment);

    @dagger.Module
    class Module {

        @Provides
        @PerFragment
        MoviesPresenter provideMoviePresenter(App app, MovieUseCase movieUseCase){
            return new MoviesPresenterImpl(app, movieUseCase);
        }
    }
}
