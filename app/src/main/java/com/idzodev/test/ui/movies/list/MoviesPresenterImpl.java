package com.idzodev.test.ui.movies.list;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.idzodev.test.core.android.App;
import com.idzodev.test.core.mvp.BasePresenter;
import com.idzodev.test.core.utils.InternetUtils;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.domain.use_cases.MovieUseCase;
import com.idzodev.test.ui.dvo.movie.GetMoviesDvo;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;
import com.idzodev.test.ui.movies.filters.FiltersFragment;

import org.parceler.Parcels;

/**
 * Implementation of {@link MoviesPresenter}
 */
public class MoviesPresenterImpl extends BasePresenter<MovieView> implements MoviesPresenter {
    private static final String SAVE_STATE_FILTER = "SAVE_STATE_FILTER";
    public static final int REQUEST_FILTERS = 1;

    private MovieUseCase movieUseCase;
    private App app;
    private MovieFilters mMovieFilters = new MovieFilters();

    public MoviesPresenterImpl(App app, MovieUseCase movieUseCase) {
        this.movieUseCase = movieUseCase;
        this.app = app;
    }

    @Override
    protected void handleError(Throwable throwable) {
        handleDefaultsErrors(app, throwable);

        if (getView() == null) return;

        getView().showProgress(false);
    }

    @Override
    protected void onViewAttached(Bundle savedInstanceState) {
        if (savedInstanceState != null){
            restoreState(savedInstanceState);
            return;
        }

        getView().showProgress(true);
        getMovies(GetMoviesDvo.LOAD_DB_API);
    }

    @Override
    public void onSave(Bundle output) {
        super.onSave(output);
        output.putParcelable(SAVE_STATE_FILTER, Parcels.wrap(mMovieFilters));
    }

    @Override
    public void onMovieClicked(MovieItemDvo item) {
    }

    @Override
    public void onRefreshClicked() {
        if (!InternetUtils.isNetworkAvailable(app)){
            showNoInternetPopup(app);
            getView().showProgress(false);
            return;
        }

        getView().showProgress(true);
        getMovies(GetMoviesDvo.LOAD_API);
    }

    @Override
    public void onFilterClicked() {
        getView().showFiltersScreen(mMovieFilters, REQUEST_FILTERS);
    }

    @Override
    public void onActivityResult(int request, int resultCode, Intent data) {
        super.onActivityResult(request, resultCode, data);
        if (request == REQUEST_FILTERS && resultCode == Activity.RESULT_OK){
            mMovieFilters = Parcels.unwrap(data.getParcelableExtra(FiltersFragment.ARGS_SELECTED_FILTERS));

            if (getView() != null){
                getView().showProgress(true);
                getMovies(GetMoviesDvo.LOAD_DB);
            }
        }
    }

    /**
     * Get movies from use case
     *
     * @param loadType loading type of movies
     * @see com.idzodev.test.ui.dvo.movie.GetMoviesDvo.LoadMovies - all loading types
     */
    private void getMovies(@GetMoviesDvo.LoadMovies int loadType){
        addDisposable(movieUseCase.getMovies(new GetMoviesDvo(mMovieFilters, loadType)).subscribe(moviesDvo -> {
            if (getView() == null) return;

            getView().showMovies(moviesDvo.getMovieItems());

            if (moviesDvo.isUpdated() || !moviesDvo.getMovieItems().isEmpty())
                getView().showProgress(false);
        }, this::handleError));
    }

    /**
     * Restore state of presenter
     *
     * @param savedInstanceState - {@link Bundle} from where restore state
     */
    private void restoreState(@NonNull Bundle savedInstanceState) {
        mMovieFilters = Parcels.unwrap(savedInstanceState.getParcelable(SAVE_STATE_FILTER));
    }
}
