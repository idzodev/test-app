package com.idzodev.test.ui.movies.list;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idzodev.test.R;
import com.idzodev.test.core.android.App;
import com.idzodev.test.core.mvp.MvpFragment;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.ui.di.MovieComponent;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;
import com.idzodev.test.ui.movies.filters.FiltersFragment;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment that shows a list of movies
 */
public class MoviesFragment extends MvpFragment<MoviesPresenter> implements MovieView {
    private static final String SAVED_STATE_RECYCLER_POSITION = "SAVED_STATE_RECYCLER_POSITION";

    @BindView(R.id.movie_fragment_list)
    protected RecyclerView vList;
    @BindView(R.id.movie_fragment_filter)
    protected FloatingActionButton vFilterBtn;
    @BindView(R.id.movie_fragment_swipe_layout)
    protected SwipeRefreshLayout vSwipeLayout;
    @BindView(R.id.toolbar)
    protected Toolbar vToolbar;

    @Inject
    protected MoviesPresenter moviesPresenter;

    private MoviesAdapter mMoviesAdapter;

    @NonNull
    @Override
    protected MoviesPresenter setupPresenter() {
        App.get(getActivity())
                .getAppComponent()
                .plus(new MovieComponent.Module())
                .inject(this);

        return moviesPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.movie_fragment, container, false);
        ButterKnife.bind(this, view);
        setupToolbar(vToolbar, R.string.movies, null, view1 -> {});

        vList.setLayoutManager(new LinearLayoutManager(getContext()));
        vList.setAdapter(mMoviesAdapter = new MoviesAdapter(getContext(), item -> getPresenter().onMovieClicked(item)));
        vSwipeLayout.setOnRefreshListener(() -> getPresenter().onRefreshClicked());
        vFilterBtn.setOnClickListener(view1 -> getPresenter().onFilterClicked());

        if (savedInstanceState != null){
            mMoviesAdapter.onRestoreState(savedInstanceState);
            vList.getLayoutManager().onRestoreInstanceState(savedInstanceState.getParcelable(SAVED_STATE_RECYCLER_POSITION));
        }

        getPresenter().attachView(this);
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_STATE_RECYCLER_POSITION, vList.getLayoutManager().onSaveInstanceState());
        mMoviesAdapter.saveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getPresenter().detachView();
    }

    @Override
    public void showMovies(List<MovieItemDvo> movieItems) {
        mMoviesAdapter.showNewItems(movieItems);
    }

    @Override
    public void showProgress(boolean show) {
        if (show){
            vFilterBtn.hide();
            if (!vSwipeLayout.isRefreshing())
                vSwipeLayout.setRefreshing(true);
        } else {
            vFilterBtn.show();
            vSwipeLayout.setRefreshing(false);
        }
    }

    @Override
    public void showFiltersScreen(MovieFilters movieFilters, int requestFilters) {
        final FiltersFragment filtersFragment = FiltersFragment.newInstance(movieFilters);
        filtersFragment.setTargetFragment(this, requestFilters);

        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .add(R.id.fragment_container, filtersFragment)
                .commit();
    }
}
