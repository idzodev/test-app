package com.idzodev.test.ui.di;

import com.idzodev.test.core.android.App;
import com.idzodev.test.core.di.scope.PerFragment;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.domain.use_cases.MovieUseCase;
import com.idzodev.test.ui.movies.filters.FiltersFragment;
import com.idzodev.test.ui.movies.filters.FiltersPresenter;
import com.idzodev.test.ui.movies.filters.FiltersPresenterImpl;

import java.util.List;

import dagger.Provides;
import dagger.Subcomponent;

/**
 * Movie filters component for {@link FiltersFragment}
 */
@PerFragment
@Subcomponent(modules = FiltersComponent.Module.class)
public interface FiltersComponent {

    void inject(FiltersFragment filtersFragment);

    @dagger.Module
    class Module {
        /**
         * Selected filters
         */
        private MovieFilters selectedFilters;

        public Module(MovieFilters selectedFilters) {
            if (selectedFilters == null) selectedFilters = new MovieFilters();
            this.selectedFilters = selectedFilters;
        }

        @PerFragment
        @Provides
        FiltersPresenter provideFiltersPresenter(App app, MovieUseCase movieUseCase){
            return new FiltersPresenterImpl(selectedFilters, movieUseCase, app);
        }
    }
}
