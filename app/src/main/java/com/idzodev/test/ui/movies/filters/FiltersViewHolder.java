package com.idzodev.test.ui.movies.filters;

import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.v7.widget.RecyclerView;
import android.widget.CheckBox;
import android.widget.TextView;

import com.idzodev.test.R;
import com.idzodev.test.core.callbacks.OnItemClickListener;
import com.idzodev.test.ui.dvo.movie.GenreItemDvo;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * ViewHolder of genre filter in list
 */
public class FiltersViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.filter_genre_item_name)
    protected TextView vName;
    @BindView(R.id.filter_genre_item_selected)
    protected CheckBox vSelected;

    public FiltersViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void show(GenreItemDvo dvo, boolean isSelected, OnItemClickListener<GenreItemDvo> genreItemDvoOnItemClickListener) {
        vName.setText(dvo.getName());
        vSelected.setChecked(isSelected);
        vSelected.setOnCheckedChangeListener((compoundButton, b) -> genreItemDvoOnItemClickListener.onItemClicked(dvo));
    }
}

