package com.idzodev.test.ui.dvo.movie;

import android.os.Parcelable;

import org.parceler.Parcel;

import java.util.List;

/**
 * Movie item data view object. This class also implemented default
 * Android {@link Parcelable} functionality by using {@link org.parceler.Parcels}.
 */
@Parcel
public class MovieItemDvo {
    protected String title;
    protected String image;
    protected float rating;
    protected int releaseYear;
    protected List<GenreItemDvo> genreItems;

    public MovieItemDvo() {
    }

    public MovieItemDvo(String title, String image, float rating, int releaseYear, List<GenreItemDvo> genreItems) {
        this.title = title;
        this.image = image;
        this.rating = rating;
        this.releaseYear = releaseYear;
        this.genreItems = genreItems;
    }

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public float getRating() {
        return rating;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public List<GenreItemDvo> getGenreItems() {
        return genreItems;
    }

    /**
     * Concat all genres to single line separated by |
     *
     * @return
     */
    public String getFormatGenreText() {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < genreItems.size(); i++) {
            if (i != 0){
                builder.append(" | ");
            }

            builder.append(genreItems.get(i).getName());
        }

        return builder.toString();
    }
}
