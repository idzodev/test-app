package com.idzodev.test.ui.movies.list;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.idzodev.test.R;
import com.idzodev.test.core.callbacks.OnItemClickListener;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter that manages a collection of {@link MovieItemDvo}.
 */
public class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {
    private final static String SAVED_STATE_ITEMS = MoviesAdapter.class.getName()+"SAVED_STATE_ITEMS";
    private final LayoutInflater inflater;

    private List<MovieItemDvo> items;
    private OnItemClickListener<MovieItemDvo> onItemClickListener;

    public MoviesAdapter(Context context, OnItemClickListener<MovieItemDvo> onItemClickListener) {
        this.inflater = LayoutInflater.from(context);
        this.items = new ArrayList<>();

        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MovieViewHolder(inflater.inflate(R.layout.movie_item, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {
        final int type = getItemViewType(position);

        MovieViewHolder holder = (MovieViewHolder) h;
        holder.show(items.get(position), position, this);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    @Override
    public void onClick(View view) {
        int pos = (int) view.getTag();
        onItemClickListener.onItemClicked(items.get(pos));
    }

    /**
     * Show new movies items and {@link RecyclerView.Adapter#notifyDataSetChanged()}
     *
     * @param dvo list of movies
     */
    public void showNewItems(List<MovieItemDvo> dvo) {
        this.items.clear();
        this.items.addAll(dvo);
        notifyDataSetChanged();
    }

    /**
     * Save adapter state
     *
     * @param outState {@link Bundle} to save state
     */
    public void saveInstanceState(@NonNull Bundle outState) {
        outState.putParcelable(SAVED_STATE_ITEMS, Parcels.wrap(items));
    }

    /**
     * Restore adapter state
     *
     * @param savedInstanceState Bundle from where restore saved state
     */
    public void onRestoreState(@NonNull Bundle savedInstanceState) {
        List<MovieItemDvo> saved = Parcels.unwrap(savedInstanceState.getParcelable(SAVED_STATE_ITEMS));
        showNewItems(saved);
    }
}

