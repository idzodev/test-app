package com.idzodev.test.ui.dvo.movie;

import java.util.List;

/**
 * Genres data view object
 */
public class GenresDvo {
    List<GenreItemDvo> genres;

    public GenresDvo(List<GenreItemDvo> genres) {
        this.genres = genres;
    }

    public List<GenreItemDvo> getGenres() {
        return genres;
    }
}
