package com.idzodev.test.ui.dvo.movie;

import android.os.Parcelable;

import org.parceler.Parcel;

/**
 * Genre item data view object. This class also implemented default
 * Android {@link Parcelable} functionality by using {@link org.parceler.Parcels}.
 */
@Parcel
public class GenreItemDvo {
    protected String id;
    protected String name;

    public GenreItemDvo() {
    }

    public GenreItemDvo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
