package com.idzodev.test.ui.movies.filters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.idzodev.test.R;
import com.idzodev.test.core.callbacks.OnItemClickListener;
import com.idzodev.test.ui.dvo.movie.GenreItemDvo;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * ViewHolder for header item in list of filters
 */
public class FiltersHeaderViewHolder extends RecyclerView.ViewHolder {

    public FiltersHeaderViewHolder(View itemView) {
        super(itemView);
    }
}

