package com.idzodev.test.ui.movies.filters;

import com.idzodev.test.core.mvp.Presenter;
import com.idzodev.test.ui.movies.list.MovieView;

/**
 * Filters {@link Presenter} that controls communication between {@link FiltersView} and use case.
 */
public interface FiltersPresenter extends Presenter<FiltersView> {

    /**
     * Handle clicking of finishing selecting filters
     */
    void onFinishSelected();
}
