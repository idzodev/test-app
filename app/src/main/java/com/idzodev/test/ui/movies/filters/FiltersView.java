package com.idzodev.test.ui.movies.filters;

import android.content.Intent;

import com.idzodev.test.core.mvp.View;
import com.idzodev.test.domain.entities.MovieFilters;
import com.idzodev.test.ui.dvo.movie.GenreItemDvo;
import com.idzodev.test.ui.dvo.movie.MovieItemDvo;
import com.idzodev.test.ui.movies.list.MoviesPresenter;

import java.util.List;

/**
 * Movies filters {@link View} that controlled by {@link FiltersView}.
 */
public interface FiltersView extends View {

    /**
     * Show genres filters and selected them
     *
     * @param movieItems list of {@link GenreItemDvo}
     * @param selectedGenres selected genres
     */
    void showGenres(List<GenreItemDvo> movieItems, List<String> selectedGenres);

    /**
     * Show entered rating filter
     *
     * @param rating rating
     */
    void showRating(float rating);

    /**
     * Show entered release year filter
     *
     * @param releaseYear release year
     */
    void showReleaseYear(int releaseYear);

    /**
     * Get selected movies filters from view
     *
     * @return instance of {@link MovieFilters} with selected filter
     */
    MovieFilters getSelectedFilters();

    /**
     * Close screen with result
     *
     * @param result - result code
     * @param data - {@link Intent} result data
     */
    void close(int result, Intent data);
}
