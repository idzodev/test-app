package com.idzodev.test.core.di.modules;


import com.idzodev.test.core.executors.PostExecutionThread;
import com.idzodev.test.core.executors.ThreadExecutor;
import com.idzodev.test.domain.mappers.ExceptionMapper;
import com.idzodev.test.domain.mappers.GenreMapper;
import com.idzodev.test.domain.mappers.MovieMapper;
import com.idzodev.test.domain.repositories.GenreRepository;
import com.idzodev.test.domain.repositories.MovieRepository;
import com.idzodev.test.domain.use_cases.MovieUseCase;
import com.idzodev.test.domain.use_cases.impl.MovieUseCaseImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Dagger module that provides objects with use case logic which will live during the application
 * lifecycle.
 */
@Module(includes = {
        RepositoriesModule.class,
        ThreadExecutorsModule.class,
        ApiModule.class
})
public class UseCaseModule {

    @Provides
    @Singleton
    MovieUseCase provideMovieUseCase(
            ThreadExecutor threadExecutor,
            PostExecutionThread postExecutionThread,
            Retrofit retrofit,
            MovieRepository movieRepository,
            ExceptionMapper exceptionMapper,
            GenreRepository genreRepository,
            GenreMapper genreMapper,
            MovieMapper movieMapper){

        return new MovieUseCaseImpl(genreRepository, genreMapper, threadExecutor, postExecutionThread, retrofit, movieRepository, exceptionMapper, movieMapper);
    }
}
