package com.idzodev.test.core.di.components;


import com.idzodev.test.core.android.App;
import com.idzodev.test.core.di.modules.ApiModule;
import com.idzodev.test.core.di.modules.AppModule;
import com.idzodev.test.core.di.modules.DataModule;
import com.idzodev.test.core.di.modules.MappersModule;
import com.idzodev.test.core.di.modules.RepositoriesModule;
import com.idzodev.test.core.di.modules.ThreadExecutorsModule;
import com.idzodev.test.core.di.modules.UseCaseModule;
import com.idzodev.test.core.executors.PostExecutionThread;
import com.idzodev.test.core.executors.ThreadExecutor;
import com.idzodev.test.ui.di.FiltersComponent;
import com.idzodev.test.ui.di.MovieComponent;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import okhttp3.OkHttpClient;

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton
@Component(modules = {
        AppModule.class,
        DataModule.class,
        ThreadExecutorsModule.class,
        ApiModule.class,
        UseCaseModule.class,
        RepositoriesModule.class,
        MappersModule.class
})
public interface AppComponent {
    @Named("PicassoOkHttp")
    OkHttpClient getPicassoOkHttp();


    MovieComponent plus(MovieComponent.Module module);
    FiltersComponent plus(FiltersComponent.Module module);
}
