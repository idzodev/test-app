package com.idzodev.test.core.rx;


import com.idzodev.test.core.executors.PostExecutionThread;
import com.idzodev.test.core.executors.ThreadExecutor;

import io.reactivex.ObservableTransformer;
import io.reactivex.schedulers.Schedulers;

/**
 * Helper class for common observable transformations
 */
public class RxTransformers {

    /**
     * Quickly subscribeOn and then observeOn
     */
    public static <T> ObservableTransformer<T, T> applySchedulers(ThreadExecutor subscribeOn, PostExecutionThread observeOn) {
        return tObservable -> tObservable.subscribeOn(Schedulers.from(subscribeOn))
                .observeOn(observeOn.getScheduler());
    }
}
