package com.idzodev.test.core.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.idzodev.test.BuildConfig;
import com.idzodev.test.R;
import com.idzodev.test.core.android.App;
import com.idzodev.test.domain.exceptions.ApiException;
import com.idzodev.test.domain.exceptions.BaseException;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;

/**
 * Base presenter class that implemented {@link Presenter} methods.
 *
 * @param <V>
 */
public abstract class BasePresenter<V extends View> implements Presenter<V> {
    private V view;
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private Bundle savedInstanceState = null;


    /**
     * Method that handle all exception caused in presenter
     *
     * @param throwable
     */
    protected abstract void handleError(Throwable throwable);

    @Override
    public final void onRestore(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

    @Override
    public final void attachView(V v) {
        view = v;
        onViewAttached(savedInstanceState);
    }

    @Override
    public void onSave(Bundle output) {}

    @Override
    public void onActivityResult(int request, int resultCode, Intent data){}

    @Override
    public final void detachView() {
        onViewDetach();
        cleanDisposables();
        view = null;
    }

    /**
     * Method that will be calling after {@link View} is attached
     *
     * @param savedInstanceState Saved instance state of presenter
     */
    protected void onViewAttached(Bundle savedInstanceState){}

    /**
     * Method that will be calling before {@link View} will be detached
     */
    protected void onViewDetach(){}

    /**
     * Method that returned attached {@link View}
     *
     * @return attached instance of {@link View}
     */
    @Nullable
    protected V getView() {
        return view;
    }

    /**
     * Convenient method to store Rx subscription {@link Disposable}
     *
     * @param disposable Subscription of {@link io.reactivex.Observable}
     */
    protected void addDisposable(Disposable disposable){
        mCompositeDisposable.add(disposable);
    }

    /**
     * Convenient method to clear all Rx subscriptions {@link Disposable}
     */
    protected void cleanDisposables(){
        if (!mCompositeDisposable.isDisposed()){
            mCompositeDisposable.dispose();
        }
    }

    /**
     * Convenient method to quick show "No Internet connection" error
     * @param app {@link App} to getting string resources if need
     */
    protected void showNoInternetPopup(App app) {
        if (getView() != null && getView() instanceof PopupView){
            PopupView popupView = (PopupView) getView();
            popupView.showInfoPopup(app.getString(R.string.no_internet_title), app.getString(R.string.no_internet_message));
        }
    }

    /**
     * Default handler of exception.
     *
     * @param app {@link App} to getting string resources if need
     * @param throwable instance of exception
     */
    protected void handleDefaultsErrors(App app, Throwable throwable){
        if (getView() != null && getView() instanceof PopupView){
            PopupView popupView = (PopupView) getView();

            if (throwable instanceof ApiException){
                ApiException exception = (ApiException) throwable;

                popupView.showInfoPopup(exception.getTitle(), exception.getMessage());
            }
        }

        if (!(throwable instanceof BaseException) && !(throwable instanceof HttpException)){
//            if (!BuildConfig.DEBUG)
//                Crashlytics.logException(throwable);
        }

        if (BuildConfig.DEBUG) Log.e("Error", "Error", throwable);
    }
}
