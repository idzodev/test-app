package com.idzodev.test.core.di.modules;


import com.google.gson.Gson;
import com.idzodev.test.core.android.App;
import com.idzodev.test.domain.mappers.ExceptionMapper;
import com.idzodev.test.domain.mappers.GenreMapper;
import com.idzodev.test.domain.mappers.MovieMapper;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides mappers objects which will live during the application lifecycle.
 */
@Module
public class MappersModule {

    @Provides
    @Singleton
    ExceptionMapper provideErrorMapper(Gson gson, App app){
        return new ExceptionMapper(gson, app);
    }

    @Provides
    @Singleton
    MovieMapper provideMovieMapper(){
        return new MovieMapper();
    }

    @Provides
    @Singleton
    GenreMapper provideGenreMapper(){
        return new GenreMapper();
    }
}
