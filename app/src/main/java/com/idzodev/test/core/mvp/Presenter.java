package com.idzodev.test.core.mvp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Pair;

import java.util.List;

/**
 * Interface representing a Presenter in a model view presenter (MVP) pattern.
 * @param <V> type of presenter View
 */
public interface Presenter<V extends View> {

    /**
     * Method that attached view to Presenter
     *
     * @param v view that attaching to presenter
     */
    void attachView(V v);

    /**
     * Method that detached view from Presenter
     */
    void detachView();

    /**
     * Called to ask the presenter to save its current dynamic state, so it can later be
     * reconstructed in a new instance of its process is restarted.
     *
     * @param output Bundle to place your state
     */
    void onSave(Bundle output);

    /**
     * This method is called when the fragment is being re-initialized from a
     * previously saved state, given here in savedInstanceState.
     *
     * @param savedInstanceState Bundle of saved state
     */
    void onRestore(Bundle savedInstanceState);

    /**
     * Receive the result from a fragment. This follows the related Activity API as described there
     * in {@link android.app.Activity#onActivityResult(int, int, Intent)} .
     *
     * @param request The integer request code originally supplied to startActivityForResult(),
     *                allowing you to identify who this result came from.
     * @param resultCode The integer result code returned by the child activity through
     *                   its setResult().
     * @param data An Intent, which can return result data to the caller (various data can
     *             be attached to Intent "extras").
     */
    void onActivityResult(int request, int resultCode, Intent data);
}
