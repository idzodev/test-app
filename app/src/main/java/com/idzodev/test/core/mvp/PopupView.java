package com.idzodev.test.core.mvp;

/**
 * Interface representing view can to show message popup
 */
public interface PopupView {

    /**
     * Method that show simple information popup
     * @param title Title of popup
     * @param message Body of popup
     */
    void showInfoPopup(String title, String message);
}
