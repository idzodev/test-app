package com.idzodev.test.core.utils;

import java.util.List;
import java.util.UUID;

/**
 * Helper class for common string operations
 */
public final class StringUtils {

    /**
     * Check if string is null or empty
     *
     * @param txt string that will be checked
     * @return true if txt is empty
     */
    public static boolean isNullEmpty(String txt){
        return txt == null || txt.isEmpty();
    }


    /**
     * Check if all strings is null or empty
     *
     * @param text string array that will be checked
     * @return true if one of string is null or empty
     */
    public static boolean isNullEmpty(String... text){
        for (String txt : text) {
            if (txt == null || txt.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Join string array to one string separated by , and wrapped by ''
     *
     * @param items list of string
     * @return joined string
     */
    public static String join(List<String> items){
        if (!items.isEmpty()) {
            StringBuilder nameBuilder = new StringBuilder();
            for (String n : items) {
                nameBuilder
                        .append("'")
                        .append(n)
                        .append("'")
                        .append(",");
            }
            nameBuilder.deleteCharAt(nameBuilder.length() - 1);
            return nameBuilder.toString();
        } else {
            return "";
        }
    }
}
