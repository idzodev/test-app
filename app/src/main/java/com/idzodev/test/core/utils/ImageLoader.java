package com.idzodev.test.core.utils;

import android.widget.ImageView;

import com.idzodev.test.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * Singleton that downloaded, cached and show images
 */
public final class ImageLoader {
    private static ImageLoader sImageLoader;

    private ImageLoader() {
    }

    /**
     * Get {@link ImageLoader} instance
     *
     * @return {@link ImageLoader} instance
     */
    public static ImageLoader getInstance() {
        if (sImageLoader == null) {
            sImageLoader = new ImageLoader();
        }
        return sImageLoader;
    }

    /**
     * Download and show movies images
     *
     * @param imageView {@link ImageView} where will be set image after it downloading
     * @param path image url
     */
    public void loadMovie(final ImageView imageView, String path){
        loadImage(
                Picasso.with(imageView.getContext()),
                path,
                R.mipmap.ic_launcher,
                imageView,
                true
        );
    }

    /**
     * Main method that download, cached and show images
     *
     * @param picasso {@link Picasso} instance
     * @param path image url
     * @param plaseholder resource of image placeholder
     * @param imageView {@link ImageView} where will be set image after it downloading
     * @param centerCrop crop image or not
     */
    private void loadImage(final Picasso picasso, final String path, final int plaseholder, final ImageView imageView, boolean centerCrop){
        if (StringUtils.isNullEmpty(path)){
            loadRes(imageView, plaseholder);
            return;
        }

        RequestCreator requestCreator =  picasso.load(path);

        requestCreator.noFade();
        requestCreator.fit();
        if (centerCrop){
            requestCreator.centerCrop();
        } else {
            requestCreator.centerInside();
        }

        if (plaseholder != 0){
            requestCreator.placeholder(plaseholder);
            requestCreator.error(plaseholder);
        }

        requestCreator.networkPolicy(NetworkPolicy.OFFLINE);
        requestCreator.into(imageView, new Callback() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError() {
                RequestCreator requestCreator =  picasso.load(path);
                requestCreator.noFade();
                requestCreator.fit();
                if (centerCrop){
                    requestCreator.centerCrop();
                } else {
                    requestCreator.centerInside();
                }
                if (plaseholder != 0) {
                    requestCreator.placeholder(plaseholder);
                    requestCreator.error(plaseholder);
                }
                requestCreator.into(imageView);
            }
        });
    }

    /**
     * Method to show big resources using cache
     *
     * @param imageView {@link ImageView} where will be set image after it downloading
     * @param res resource of image
     */
    public void loadRes(ImageView imageView, int res){
        Picasso.with(imageView.getContext()).load(res).into(imageView);
    }
}
