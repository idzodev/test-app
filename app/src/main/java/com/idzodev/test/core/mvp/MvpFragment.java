package com.idzodev.test.core.mvp;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.idzodev.test.core.android.BaseFragment;
import com.idzodev.test.core.mvp.Presenter;
import com.idzodev.test.core.mvp.View;

/**
 * Base Fragment that helps to organize MVP pattern inside Fragments.
 *
 * @param <P> Presenter type extended from {@link Presenter}. This will be main Presenter
 *           for current fragment instance
 */
public abstract class MvpFragment<P extends Presenter> extends BaseFragment implements View {

    private P presenter;

    /**
     * Method that forces all children to setup and return NonNull Presenter.
     * @return Presenter for current fragment instance.
     */
    @NonNull
    protected abstract P setupPresenter();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getPresenter();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getPresenter().onSave(outState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPresenter().onRestore(savedInstanceState);
    }

    @Nullable
    @Override
    public android.view.View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getPresenter().onRestore(savedInstanceState);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        getPresenter().onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Method that quick show message to user
     * @param title title of message
     * @param message body of message
     */
    public void showInfoPopup(String title, String message){
        Toast.makeText(getContext(),  title, Toast.LENGTH_SHORT).show();
    }

    /**
     * Method that get access to Fragment Presenter. If Presenter does not setting up it will
     * call {@link #setupPresenter()} to configure Presenter.
     * @return fragment Presenter
     */
    @NonNull
    public P getPresenter(){
        if (presenter == null)
            presenter = setupPresenter();
        return presenter;
    }

    /**
     * Convenient method to setup toolbar if it's inside fragment
     *
     * @param toolbar fragment toolbar
     * @param text string resource address of toolbar title text
     * @param icon toolbar navigation icon
     * @param onNavigationClickListener listener of toolbar navigation button
     */
    protected void setupToolbar(@NonNull Toolbar toolbar,@NonNull @StringRes Integer text,@Nullable @DrawableRes Integer icon, @Nullable android.view.View.OnClickListener onNavigationClickListener){
        setupToolbar(toolbar, getContext().getString(text), icon, onNavigationClickListener);
    }

    /**
     * Convenient method to setup toolbar if it's inside fragment
     *
     * @param toolbar fragment toolbar
     * @param text toolbar title text
     * @param icon toolbar navigation icon
     * @param onNavigationClickListener listener of toolbar navigation button
     */
    protected void setupToolbar(
            @NonNull Toolbar toolbar,
            @Nullable String text,
            @Nullable @DrawableRes Integer icon,
            @Nullable android.view.View.OnClickListener onNavigationClickListener
    ){
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        if (text != null)
            toolbar.setTitle(text);
        if (icon != null)
            toolbar.setNavigationIcon(icon);
        toolbar.setNavigationOnClickListener(onNavigationClickListener);
    }

    /**
     * Convenient method to handle {@link AppCompatActivity#onBackPressed()} called from activity
     */
    public void onBackPressed() {
        if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
            getActivity().getSupportFragmentManager()
                .popBackStack();
    }
}
