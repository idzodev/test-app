package com.idzodev.test.core.android;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.idzodev.test.BuildConfig;
import com.idzodev.test.R;
import com.idzodev.test.core.di.components.AppComponent;
import com.idzodev.test.core.di.components.DaggerAppComponent;
import com.idzodev.test.core.di.modules.ApiModule;
import com.idzodev.test.core.di.modules.AppModule;
import com.idzodev.test.core.di.modules.DataModule;
import com.idzodev.test.core.di.modules.MappersModule;
import com.idzodev.test.core.di.modules.ThreadExecutorsModule;
import com.idzodev.test.core.utils.ImageLoader;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import okhttp3.HttpUrl;

/**
 * Main Application
 */
public class App extends Application {

    private AppComponent appComponent;

    public static App get(@NonNull Context context) {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        setupAppComponent();
        setupPicasso();
    }

    @NonNull
    public AppComponent getAppComponent() {
        return appComponent;
    }

    /**
     * Setup Application dependencies
     */
    private void setupAppComponent(){
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .apiModule(new ApiModule(HttpUrl.parse(getString(R.string.base_url))))
                .threadExecutorsModule(new ThreadExecutorsModule())
                .dataModule(new DataModule())
                .mappersModule(new MappersModule())
                .build();

    }

    /**
     * Setup Picasso images loader
     */
    private void setupPicasso(){
        Picasso picasso = new Picasso.Builder(this)
                .downloader(new OkHttp3Downloader(appComponent.getPicassoOkHttp()))
                .loggingEnabled(BuildConfig.DEBUG)
                .build();

        Picasso.setSingletonInstance(picasso);
        ImageLoader.getInstance();
    }
}
