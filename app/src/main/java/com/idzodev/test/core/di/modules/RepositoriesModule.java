package com.idzodev.test.core.di.modules;

import com.idzodev.test.domain.entities.DaoSession;
import com.idzodev.test.domain.mappers.MovieMapper;
import com.idzodev.test.domain.repositories.GenreRepository;
import com.idzodev.test.domain.repositories.MovieRepository;
import com.idzodev.test.domain.repositories.SaveEntityRepository;
import com.idzodev.test.domain.repositories.impl.GenreRepositoryImpl;
import com.idzodev.test.domain.repositories.impl.MovieRepositoryImpl;
import com.idzodev.test.domain.repositories.impl.SaveEntityRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


/**
 * Dagger module that provides repositories objects which will live during the application lifecycle.
 */
@Module(includes = {
        DataModule.class,
        MappersModule.class
})
public class RepositoriesModule {

    @Provides
    @Singleton
    SaveEntityRepository provideSaveEntityRepository(
            DaoSession daoSession,
            MovieMapper movieMapper
    ){
        return new SaveEntityRepositoryImpl(daoSession, movieMapper);
    }

    @Provides
    @Singleton
    MovieRepository provideMovieRepository(SaveEntityRepository saveEntityRepository){
        return new MovieRepositoryImpl(saveEntityRepository);
    }
    @Provides
    @Singleton
    GenreRepository provideGenreRepository(SaveEntityRepository saveEntityRepository){
        return new GenreRepositoryImpl(saveEntityRepository);
    }

}
