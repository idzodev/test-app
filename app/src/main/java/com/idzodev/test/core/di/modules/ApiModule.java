package com.idzodev.test.core.di.modules;

import android.content.res.Resources;
import android.util.Log;

import com.google.gson.Gson;
import com.idzodev.test.BuildConfig;
import com.idzodev.test.core.android.App;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Dagger module that provides objects using in API calling which will live during the application
 * lifecycle.
 */
@Module
public class ApiModule {
    private HttpUrl httpUrl;

    public ApiModule(HttpUrl httpUrl) {
        this.httpUrl = httpUrl;
    }

    @Provides
    @Singleton
    @Named("SimpleOkHttp")
    OkHttpClient provideSimpleOkHttpClient(){
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return new OkHttpClient
                .Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .followRedirects(true)
                .followSslRedirects(true)
                .addInterceptor(interceptor)
                .build();
    }


    @Provides
    @Singleton
    @Named("PicassoOkHttp")
    OkHttpClient providePicassoOkHttpClient(App app){
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(
                message -> Log.d("picasso", message, null));
        interceptor.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        return new OkHttpClient
                .Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .followRedirects(true)
                .followSslRedirects(true)
                .addInterceptor(interceptor)
                .addNetworkInterceptor(chain -> {
                    Response originalResponse = chain.proceed(chain.request());
                    return originalResponse
                            .newBuilder()
                            .header("Cache-Control", "max-age=" + (60 * 60 * 24 * 365))
                            .build();
                })
                .cache(new Cache(app.getCacheDir(), Integer.MAX_VALUE))
                .build();
    }

    @Provides
    @Singleton
    Gson provideGson(){
        return new Gson();
    }


    @Singleton
    @Provides
    Retrofit provideSimpleRetrofit(@Named("SimpleOkHttp") OkHttpClient okHttpClient, Gson gson){
        return new Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(httpUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }
}
