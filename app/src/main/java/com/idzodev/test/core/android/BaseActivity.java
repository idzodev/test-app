package com.idzodev.test.core.android;

import android.support.v7.app.AppCompatActivity;


/**
 * Base {@link android.support.v7.app.AppCompatActivity} class for every Activity in this application.
 */
public abstract class BaseActivity extends AppCompatActivity {

}
