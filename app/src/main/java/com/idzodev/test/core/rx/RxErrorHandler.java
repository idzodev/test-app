package com.idzodev.test.core.rx;


import com.idzodev.test.domain.mappers.ExceptionMapper;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;

/**
 * Helper class for observable transformations
 */
public class RxErrorHandler {

    /**
     * Handle all exceptions and convert it to {@link com.idzodev.test.domain.exceptions.BaseException}
     *
     * @param errorMapper Mapper that will convert throwable to BaseException
     * @param <T> type of Observable
     * @return Observable with error
     */
    public static <T> ObservableTransformer<T, T> handleError(ExceptionMapper errorMapper) {
        return tObservable -> tObservable.onErrorResumeNext(throwable -> {
            return Observable.error(errorMapper.handleError(throwable));
        });
    }
}
