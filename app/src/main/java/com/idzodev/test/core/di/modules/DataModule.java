package com.idzodev.test.core.di.modules;



import com.idzodev.test.core.android.App;
import com.idzodev.test.domain.entities.DaoMaster;
import com.idzodev.test.domain.entities.DaoSession;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provide {@link DaoSession} instance for working this DB which will live
 * during the application lifecycle.
 */
@Module
public class DataModule {

    @Provides
    @Singleton
    public DaoSession provideSession(App app){
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(app, "test_app_db", null);
        DaoMaster master = new DaoMaster(helper.getWritableDatabase());
        return master.newSession();
    }
}
