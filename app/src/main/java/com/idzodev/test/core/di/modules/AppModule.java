package com.idzodev.test.core.di.modules;



import com.idzodev.test.core.android.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides {@link App} instance which will live during the application lifecycle.
 */
@Module
public class AppModule {

    private App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Singleton
    @Provides
    public App provideAppContext(){
        return app;
    }
}
