package com.idzodev.test.core.callbacks;

/**
 * Clicking data callback
 * @param <T> type of clicking data value from {@link #onItemClicked} method.
 */
public interface OnItemClickListener<T> {

    /**
     * Method to handle clicking
     *
     * @param item clicking data
     */
    void onItemClicked(T item);
}
