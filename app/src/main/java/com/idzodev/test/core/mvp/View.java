package com.idzodev.test.core.mvp;

/**
 * Interface representing a View in a model view presenter (MVP) pattern.
 */
public interface View {
}
