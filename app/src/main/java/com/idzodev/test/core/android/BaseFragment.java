package com.idzodev.test.core.android;

import android.support.v4.app.Fragment;

/**
 * Base {@link android.support.v7.app.AppCompatActivity} class for every Fragment in this application.
 */
public abstract class BaseFragment extends Fragment{
}
