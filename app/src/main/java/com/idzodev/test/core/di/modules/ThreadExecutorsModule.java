package com.idzodev.test.core.di.modules;



import com.idzodev.test.core.executors.PostExecutionThread;
import com.idzodev.test.core.executors.ThreadExecutor;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides thread executors which will live during the application lifecycle.
 */
@Module
public class ThreadExecutorsModule {

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(){
        return new ThreadExecutor.DefaultWorker();
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread (){
        return new PostExecutionThread.DefaultWorker();
    }
}
